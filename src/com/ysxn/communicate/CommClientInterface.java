package com.ysxn.communicate;
/**
 * 通信客户端接口
 * @author stronghill
 *
 */
public interface CommClientInterface extends DataCommInterface {

	/**
	 * 连接服务端
	 * @param ip 服务端ip
	 * @param port 服务端端口
	 * @return 0，连接成功，小于0，失败
	 */
	public int connect(String ip, int port);
	/**
	 * 判断是否已连接到服务端
	 * @return 是否已连接
	 */
	public boolean isConnected();
	/**
	 * 关闭连接
	 * @return 0，关闭成功，小于0，失败
	 */
	public int close();
	/**
	 * 检查上次收到数据时间，如果大于30分钟没有收到数据，就关闭连接
	 * @return 0，未超时；1，超时，并已关闭；其他，关闭失败
	 */
	public int checkLastRecvTime();
	/**
	 * 检查如果大于这个分钟数没有收到终端信息，就认为终端掉线，系统踢掉socket
	 */
	public static int CHECKOFFMIN = 30;
}