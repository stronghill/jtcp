package com.ysxn.communicate.socket;

import com.jtcp.MainFrame;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;    
import org.apache.commons.logging.LogFactory;  

import com.ysxn.communicate.CommClientInterface;
import com.ysxn.communicate.CommServerInterface;

/**
 * ͨ
 *
 * @author stronghill
 *
 */
public class CommServerImpl implements CommServerInterface {

    private ServerSocket mss = null;
    private List<CommClientInterface> clist = null;
    private static final Log log = LogFactory.getLog(CommServerImpl.class);
    private MainFrame mf = null;

    /**
     *
     * @param port
     * @return
     */
    public int init(String ip, int port) {
        try {
            mss = new ServerSocket();
            mss.bind(new InetSocketAddress(ip, port));

            SocketServerThread sst = new SocketServerThread(mss);
            sst.setMf(mf);
            sst.start();
            clist = new ArrayList<CommClientInterface>();

        } catch (Exception e) {
            log.error("server init error", e);
            return -1;
        }
        return 0;
    }

    public List<CommClientInterface> getClientList() {
        return clist;
    }

    /**
     * @return the mf
     */
    public MainFrame getMf() {
        return mf;
    }

    /**
     * @param mf the mf to set
     */
    public void setMf(MainFrame mf) {
        this.mf = mf;
    }

}
