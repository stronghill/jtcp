package com.ysxn.communicate.socket;

import com.jtcp.MainFrame;
import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

import com.ysxn.communicate.CommClientInterface;
import com.ysxn.communicate.CommFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SocketServerThread extends Thread {

    private static final Log log = LogFactory.getLog(SocketServerThread.class);
    private MainFrame mf = null;

    private Socket client;
    private ServerSocket ss = null;

    public SocketServerThread(ServerSocket server) throws IOException {
        this.ss = server;

    }

    public void run() {
        try {
            while (true) {
                try {
                    client = ss.accept();
                    CommFactory cf = new CommFactory();
                    CommClientInterface ci = cf.getCommClient();
                    CommClientImpl cil = (CommClientImpl) ci;
                    cil.setClient(client);
                    cil.setIdTerminal("server");
                    SocketClientThread cthread = new SocketClientThread(cil);
                    cthread.start();
                    
                    cil.setMf(mf);
                    mf.recvClient(cil);

                } catch (Exception e) {
                    log.error("SocketServerThread received error 600", e);
                }
            }
        } catch (Exception e) {
            log.error("SocketServerThread received error 601", e);
        }
    }

    /**
     * @return the mf
     */
    public MainFrame getMf() {
        return mf;
    }

    /**
     * @param mf the mf to set
     */
    public void setMf(MainFrame mf) {
        this.mf = mf;
    }

}
