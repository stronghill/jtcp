/**
 * 数据传输接口
 */
package com.ysxn.communicate;
/**
 * @author stronghill
 *
 */
public interface DataCommInterface {
	/**
	 * 处理接收数据的回调函数
	 * @param data 收到的数据
	 * @param len 收到的数据的长度
	 * @return
	 */
	public int onReceived(byte[] data, int len);
	/**
	 * 客户端实现时，异步调用通用的发送方法，把自己实现的数据处理接口当作参数交给系统，系统收到返回数据后自动回调onReceived方法
	 * @param data 要发送的数据
	 * @param off 要发送的数据起始字节,从0开始
	 * @param len 要发送的数据的长度
	 * @return 发送byte串长度，小于0表示失败
	 */
	public int send(byte[] data, int off, int len);
	
	/**
	 * 同步发送数据，返回收到的数据
	 * @param data 要发送的数据
	 * @param off 要发送的数据起始字节,从0开始
	 * @param len 要发送的数据的长度
	 * @return
	 */
	public byte[] sendSync(byte[] data, int off, int len);

}