package com.ysxn.communicate;

import com.ysxn.communicate.socket.CommClientImpl;
import com.ysxn.communicate.socket.CommServerImpl;
/**
 * commClientImpl工厂类
 * @author Administrator
 *
 */
public class CommFactory {
	/**
	 * 返回commClientImpl实例对象
	 * @return
	 */
	public CommClientInterface getCommClient(){
		return new CommClientImpl();
	}
	/**
	 * 返回CommServerImpl实例对象
	 * @return
	 */
	public CommServerInterface getCommServer(){
		return new CommServerImpl();
	}
	
}