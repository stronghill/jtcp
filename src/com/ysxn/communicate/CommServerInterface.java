package com.ysxn.communicate;

import java.util.List;

/**
 * 
 * @author stronghill
 *
 */
public interface CommServerInterface {
	/**
	 * 
	 * @param port 
	 * @return
	 */
	public int init(String ip,int port);
	
	public List<CommClientInterface> getClientList(); 

	//public CommClientInterface 
}
