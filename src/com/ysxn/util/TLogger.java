package com.ysxn.util;


import java.io.File;
import java.io.FileOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 记录网络通信内容得日志工具
 * @author stronghill
 *
 */
public class TLogger {
	private static TLogger instance = new TLogger();
        private static final Log log = LogFactory.getLog(TLogger.class);
	private TLogger(){
		
	}
	public static TLogger getInstance(){
		return instance;
	}
	/**
	 * 记录日志
	 * @param path 文件存放路径
	 * @param id 终端id
	 * @param tmsg 通信连接信息（socketid）
	 * @param data 通信内容
	 * @return
	 */
	public int log(String path, String id, String tmsg, String data){
		String applicationPath=System.getProperty("user.dir");
		//applicationPath=applicationPath.substring(6,applicationPath.length());
		String rpath = applicationPath + path;
		String filename = rpath + GTools.getDayStr2(0) + "/"  + id + "_" + GTools.getDayStr2(0) + ".log";
		File file=new File(filename);
		FileOutputStream writefile=null;
		try{
			if(file.exists()){
				writefile=new FileOutputStream(file,true);
			}else{
				File filepath=new File(filename.substring(0,filename.lastIndexOf("/")));
				filepath.mkdirs();
				file.createNewFile();
				writefile=new FileOutputStream(file);
			}
			writefile.write((GTools.getDateTimeStr2(0) + "_" + tmsg + ":").getBytes("GBK"));
			writefile.write(data.getBytes("GBK"));
			writefile.write('\n');
			writefile.close();
		}catch(Exception ee){
			log.error("写日志失败!filename=" + filename, ee);
		}
		return 0;
	}
	public final static String PATH_FUKONG = "/./logs/fukong/";
	public final static String PATH_KEMEI = "/./logs/kemei/";
	public final static String PATH_XINHONGJI = "/./logs/xinhongji/";
	public final static String PATH_3761 = "/./logs/3761/";
	public final static String PATH_1302004 = "/./logs/1302004/";
	public final static String PATH_1302005 = "/./logs/1302005/";
	public final static String PATH_NEIMENGFUKONG = "/./logs/neimengfukong/";
	public final static String PATH_GAORUIDA = "/./logs/gaoruida/";
	public final static String PATH_UNKNOWN = "/./logs/unknown/";
	public final static String PATH_PHONE = "/./logs/phone/";
	public final static String PATH_DIAMLIANYU = "/./logs/dianlianyu/";
	public final static String PATH_XIAOCHENG_XDS = "/./logs/xiaochengxds/";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
