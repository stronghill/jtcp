package com.ysxn.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class GTools {
	/**
	 * 得到格式化的日期
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyy-MM-dd 00:00:00"
	 */
	public static String getDayStr(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat(
				"yyyy-MM-dd 00:00:00");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	
	/**
	 * 得到格式化的日期
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyyMMdd"
	 */
	public static String getDayStr2(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	
	/**
	 * 得到格式化的日期
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyy-MM-dd"
	 */
	public static String getDayStr3(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	/**
	 * 得到格式化的日期时间
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyy-MM-dd HH:mm:ss"
	 */
	public static String getDateTimeStr(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	/**
	 * 得到格式化的日期时间2
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyyMMddHHmmss"
	 */
	public static String getDateTimeStr2(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	
	/**
	 * 得到格式化的日期时间
	 * @param offday 日期偏移量 （-1表示前一天）
	 * @return "yyyyMMddHHmm"
	 */
	public static String getDateTimeStr3(int offday){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + offday);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
		Date lastday = calendar.getTime();
		return format.format(lastday);
	}
	
	
}
